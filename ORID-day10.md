### 1.Objectives

1. Today, I briefly reviewed the concept of DTO, and its essence is actually an object-entity mapping.
2. I reviewed the related concepts of mapper, that is, when we interact with data in the front end and the back end, and the back end and the database, the data format and data content may be different. This morning, simulate this scenario by manually creating EmployeeRequest and EmployResponse.
3. I can make a phased summary through Retrospective to better integrate myself into the team and learn efficiently.
4. Learning the use of flayway plug-ins, his essence is actually similar to git, the difference is that it is a database version management plug-in.
5. As for today's Container session, I think we have not prepared enough and the explanation is too rigid. Next time, we need to put more pictures in the PPT and rehearse several times.
6. I will be more active in the preparation of the following session, especially in the rehearsal of PPT content design and explanation.

### 2.Reflective

​	 presentation ability improved and gainful

### 3. Interpretive

​	What impressed me most today is that in the Session sharing of the three groups, the PPT content of the third group is very brief, most of it is displayed in the form of pictures, and the presenter is also completely giving a speech without notes, the whole process is very smooth, which is what our group needs to learn.

### 4. Decisional

    learn to better teamwork and do better on presentation.
