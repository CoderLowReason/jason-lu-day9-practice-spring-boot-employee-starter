package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

import java.util.Optional;

public class CompanyMapper {
    public static Company toEntity(CompanyRequest CompanyRequest) {
        Company Company = new Company();
        BeanUtils.copyProperties(CompanyRequest, Company);
        return Company;
    }

    public static CompanyRequest toCompanyRequest(Company Company) {
        CompanyRequest companyRequest = new CompanyRequest();
        BeanUtils.copyProperties(Company, companyRequest);
        return companyRequest;
    }

    public static CompanyResponse toResponse(Company savedCompany) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(savedCompany, companyResponse);
        Optional.ofNullable(savedCompany.getEmployees()).ifPresent(employees -> companyResponse.setEmployeeCount(employees.size()));
        return companyResponse;
    }
}
