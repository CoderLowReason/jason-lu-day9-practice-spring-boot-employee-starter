package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.repository.JpaCompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    private final JpaCompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JpaCompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pagination = PageRequest.of(page - 1, size);
        return jpaCompanyRepository.findAll(pagination).getContent();
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        return CompanyMapper.toResponse(company);
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Optional<Company> companyTobeUpdate = jpaCompanyRepository.findById(id);
        // just to fit DTO
        Company entity = CompanyMapper.toEntity(companyRequest);
        companyTobeUpdate.ifPresent(previousCompany -> {
            previousCompany.setName(entity.getName());
            jpaCompanyRepository.save(companyTobeUpdate.get());
        });
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company entity = CompanyMapper.toEntity(companyRequest);
        Company saved = jpaCompanyRepository.save(entity);
        return CompanyMapper.toResponse(saved);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).map(Company::getEmployees).orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
        jpaEmployeeRepository.deleteByCompanyId(id);
    }
}
