package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private JPAEmployeeRepository jpaEmployeeRepository;

    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll();
    }

    public EmployeeResponse findById(Long id) {
        Employee employee = jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }

    public void update(Long id, EmployeeRequest employee) {
        Employee toBeUpdatedEmployee = jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender);
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee savedEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pagination = PageRequest.of(page - 1, size);
        return jpaEmployeeRepository.findAll(pagination).getContent();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
