package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JPAEmployeeRepository extends JpaRepository<Employee, Long> {


    List<Employee> findByGender(String gender);

    void deleteByCompanyId(Long id);

    Optional<Object> findByCompanyId(Long id);
}
